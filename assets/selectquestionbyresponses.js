/**
 * @author Denis Chenu <https://sondages.pro
 * @version 0.7.3
 */
/*jshint esversion: 6 */
function SelectQuestionByResponses(sgq, options) {
    if (!$('#responsesautocomplete' + sgq).length) {
        return;
    }
    /* get current filter in data */
    let filter = {};
    $('#responseautocomplete-' + sgq + '-dynamicFilter .filter').each(function () {
        filter[$(this).attr('id')] = $(this).text().trim();
    });
    $('#responseautocomplete-' + sgq + '-dynamicFilter').data('previousfilter', $('#responseautocomplete-' + sgq + '-dynamicFilter').html());
    $('#responsesautocomplete' + sgq).select2({
        ajax: {
            url: options.serviceUrl,
            data: function (params) {
                let filter = {};
                $('#responseautocomplete-' + sgq + '-dynamicFilter .filter').each(function () {
                    filter[$(this).attr('id')] = $(this).text().trim();
                });
                params = $.extend(params, filter);
                return params;
            },
            dataType: 'json',
            delay: 400
        },
        allowClear: true,
        minimumInputLength: options.minimumInputLength,
        placeholder: options.placeholder,
        multiple: options.multiple,
        language: options.language,
        escapeMarkup: function (markup) { return markup; },
        templateSelection: function (data) {
          if (data.group_text) {
            return "<strong>[" + data.group_text + "]</strong> " + data.text;
          }
          return data.text;
        }
    });

    $('#responsesautocomplete' + sgq).on('select2:select', function (e) {
        let data = e.params.data;
        let basename = $(this).data('basename');
        let destination = $("#" + $(this).data('destination'));
        let idInput = $(destination).find("[name^='" + basename + "']").first();
        if ($(this).prop('multiple')) {
            var multipleseparator = $(this).data('multipleseparator');
            SelectQuestionByResponsesMultipleAdd(idInput, data.id, multipleseparator);
            $.each(data, function (index, value) {
                if (index != 'id' && $("#answer" + basename + index).length) {
                    SelectQuestionByResponsesMultipleAdd("#answer" + basename + index, value, multipleseparator);
                }
            });
        } else {
            $(idInput).val(data.id).trigger("change");
            $.each(data, function (index, value) {
                if ($("#answer" + basename + index).length) {
                    $("#answer" + basename + index).val(value).trigger("change");
                }
            });
        }
    });
    $('#responsesautocomplete' + sgq).on('select2:clear', function (e) {
        let basename = $(this).data('basename');
        let destination = $("#" + $(this).data('destination'));
        let idInput = $(destination).find("[name^='" + basename + "']").first();
        $('#responseautocomplete' + sgq).find("option").remove();
        $(destination).find("[name^='" + basename + "']").each(function () {
            $(this).val("").trigger("change");
        });
    });
    $('#responsesautocomplete' + sgq).on('select2:unselect', function (e) {
        let data = e.params.data;
        if ($('#responsesautocomplete' + sgq).find("option[value='" + data.id + "'][data-extradata]").length) {
            let extradata = $('#responsesautocomplete' + sgq).find("option[value='" + data.id + "']").data('extradata');
            $.extend(data, extradata);
            $('#responsesautocomplete' + sgq).find("option[value='" + data.id + "']").remove();
        }
        let basename = $(this).data('basename');
        let destination = $("#" + $(this).data('destination'));
        let idInput = $(destination).find("[name^='" + basename + "']").first();
        if ($(this).attr('multiple')) {
            let multipleseparator = $(this).data('multipleseparator');
            let removeIndex = SelectQuestionByResponsesMultipleRemoveId(idInput, data.id, multipleseparator);
            $.each(data, function (index, value) {
                if (index != 'id' && $("#answer" + basename + index).length) {
                    SelectQuestionByResponsesMultipleRemoveByIndex("#answer" + basename + index, removeIndex, multipleseparator);
                }
            });
        } else {
            $(idInput).val("").trigger("change");
            $.each(data, function (index, value) {
                if ($("#answer" + basename + index).length) {
                    $("#answer" + basename + index).val("").trigger("change");
                }
            });
        }
    });
    //~ /* When dynamic filter is updated : reset dropdown */
    $('#responseautocomplete-' + sgq + '-dynamicFilter').data('previoushtml', $('#responseautocomplete-' + sgq + '-dynamicFilter').html());
    $('#responseautocomplete-' + sgq + '-dynamicFilter').on('html:updated', function () {
        if ($(this).html() == $(this).data('previoushtml')) {
            return;
        }
        $(this).data('previoushtml', $(this).html());
        $('#responsesautocomplete' + sgq).val(null).trigger('change');
        $('#responsesautocomplete' + sgq).empty().trigger("change");
        $('#responsesautocomplete' + sgq).trigger('select2:clear');
    });
}
/**
 * Add an element to an input
 */
function SelectQuestionByResponsesMultipleAdd(input, value, separator) {
    let Vals = $(input).val().split(separator);
    Vals = Vals.filter(val => val != "");
    Vals.push(value);
    $(input).val(Vals.join(separator)).trigger("change");
}
/**
 * Remove an element from an input
 */
function SelectQuestionByResponsesMultipleRemoveId(input, id, separator) {
    let Vals = $(input).val().split(separator);
    let index = Vals.indexOf(id);
    Vals.splice(index, 1);
    $(input).val(Vals.join(separator)).trigger("change");
    return index;
}
/**
 * Remove an element from an input
 */
function SelectQuestionByResponsesMultipleRemoveByIndex(input, index, separator) {
    let Vals = $(input).val().split(separator);
    Vals.splice(index, 1);
    $(input).val(Vals.join(separator)).trigger("change");
}
