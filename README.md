# SelectQuestionByResponses

Get a list of answers with auto-complete by responses in another survey.

You can find a demonstration at [SelectQuestionByResponses demo survey](https://demo.sondages.pro/699888) : usage to have a database of town.

## Usage

Create a survey with the question code you want for extra information or filter (only single response question (short text, huge text, single choice, equation …), use VV import to import your data in this survey, or use any way to fill data.

Create a multiple short text question, add one subquestion to get the identifier. Other sub-questions receive answer data by matching the question code.

Use the 1st survey for autocomplet the data. The identifier are the response id for a single survey, the survey id and the response id join with `-` for multiple surveys
Update the settings of the question

- _selectQuestionByResponseSurvey_ : Surveys(s) where to get responses using the survey id.
    - To have multiple survey : separate each survey id by comma `,`.
    - In this case you can add sid and srid to get the survey id and the response id.
- _selectQuestionByResponseTokenUsage_ : Usage of the token (if exist).
- _selectQuestionByResponseQuestion_ : The question code used for the text in the search. Must be a text question, single choice question or an equation.
- _selectQuestionByResponseFilters_ : Add a fixed filter during searrch of the response. One field by line using question code (limited to single question type). Field and value are separated by colon (<code>:</code>), you can use Expression Manager in value.
- _selectQuestionByResponseFiltersContain_ : Question field for contain filter. Search the value with contain. Field and value are separated by colon (<code>:</code>), you can use Expression Manager in value.
- _selectQuestionByResponseFiltersContainSeparator_ : Allow to have a separator for contains value. This allow to search a specific value in a list with a specific separator
- _selectQuestionByResponseDynamicFilters_ : Question fields for filters, checked dynamically. One field by line, field must be a valid question code (single question only). The value checked can be updated in same page by expression manager.
- _selectQuestionByResponsePlaceholder_ : the placehoder text when input was empty, default is translated “Search”
- _selectQuestionByResponseMinChar_ : Minimum character for auto search, default is 3.
- _selectQuestionByResponseMultiple_ : Alows to select multipe response. You have to choose the separator.
- _selectQuestionByResponseMultipleSeparator_ : The separator used for multiple response.

## Home page & Copyright
- HomePage <https://sondages.pro/>
- Copyright © 2022-2024 Denis Chenu <https://sondages.pro>
- Copyright © 2022 OECD <https://oecd.org>
- [Support](https://support.sondages.pro)
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 

Distributed under [GNU AFFERO GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/agpl.txt) licence
