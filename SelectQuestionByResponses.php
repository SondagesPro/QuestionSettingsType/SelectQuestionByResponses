<?php

/**
 * SelectQuestionByResponses get list of answers in another survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022-2024 Denis Chenu <www.sondages.pro>
 * @copyright 2022-2024 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license GPL v3
 * @version 0.7.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class SelectQuestionByResponses extends PluginBase
{
    protected static $description = 'Use a text question as a dropdown filled by response in another survey';
    protected static $name = 'SelectQuestionByResponses';

    protected $storage = 'DbStorage';

    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        $this->subscribe('beforeQuestionRender');
        $this->subscribe('newQuestionAttributes');
        /* Return the json */
        $this->subscribe('newDirectRequest');
        /* Check if user have sufficient right on related survey */
        $this->subscribe('beforeQuestionAttributeSave');
    }

    /**
    * The attribute
    */
    public function newQuestionAttributes()
    {
        $newAttributes = array(
            'selectQuestionByResponseSurvey' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 100,
                'inputtype' => 'text',
                'default' => "",
                'help' => $this->gT('You can get multiple survey with coma (,) as separator. Remind all test was done usig the question code. Then each surveys must have this question code. In this case : you can use `sid` to have the survey id, and `srid` to have the response id.'),
                'caption' => $this->gT('Survey(s) where to get responses'),
            ),
            /* TODO : See help and detail accordng to token table and to anonymous ... */
            'selectQuestionByResponseTokenUsage' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 110, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    'no' => gT('No'),
                    'token' => gT('Yes'),
                    'group' => gT('Token Group (with responseListAndManage plugin)')
                ),
                'default' => 'token',
                'help' => $this->gT('If you have responseListAndManage, the response list can be found using the group of current token. If the related survey have a token table : token stay mandatory.'),
                'caption' => $this->gT('Usage of token.'),
            ),
            'selectQuestionByResponseQuestion' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 130, /* Own category */
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->gT('This can be text question type, single choice question type or equation question type.'),
                'caption' => $this->gT('The question code used for the text for the label.'),
            ),
            'selectQuestionByResponseFilters' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 150, /* Own category */
                'inputtype' => 'textarea',
                'default' => "",
                'expression' => 1,
                'help' => $this->gT('One field by line, field must be a valid question code (single question only). Field and value are separated by colon (<code>:</code>), you can use Expression Manager in value.'),
                'caption' => $this->gT('Other question fields for filters.'),
            ),
            'selectQuestionByResponseFiltersContain' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 155, /* Own category */
                'inputtype' => 'text',
                'default' => "",
                'expression' => 0,
                'help' => $this->gT('Search the value with contain. Field and value are separated by colon (<code>:</code>), you can use Expression Manager in value.'),
                'caption' => $this->gT('Question field for contain filter.'),
            ),
            'selectQuestionByResponseFiltersContainSeparator' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 156, /* Own category */
                'inputtype' => 'text',
                'default' => "",
                'expression' => 0,
                'help' => $this->gT('You can add an optionnal character for separator, this allow better restriction.'),
                'caption' => $this->gT('Separator between contain value.'),
            ),
            'selectQuestionByResponseDynamicFilters' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 160, /* Own category */
                'inputtype' => 'textarea',
                'default' => "",
                'expression' => 1,
                'help' => $this->gT('One field by line, field must be a valid question code (single question only). Warning this allow the value to be set by anything without control.'),
                'caption' => $this->gT('Other question fields for filters, checked dynamically.'),
            ),
            'selectQuestionByResponseDynamicFiltersDisableEmpty' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 165, /* Own category */
                'inputtype' => 'switch',
                'default' => false,
                'help' => $this->gT('If the value of dynamic filters is empty : do not filter. Else return only the empty value for the related responses.'),
                'caption' => $this->gT('Do not use empty value'),
            ),
            'selectQuestionByResponsePlaceholder' => array(
                'types' => 'Q',
                'i18n' => true,
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 200,
                'inputtype' => 'text',
                'default' => "",
                'help' => $this->gT('The place holder, default to translated “Search”'),
                'caption' => $this->gT('Place holder on the input'),
            ),
            'selectQuestionByResponseMinChar' => array(
                'types' => 'Q',
                'i18n' => false,
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 210,
                'inputtype' => 'integer',
                'min' => 0,
                'default' => "",
                'help' => $this->gT('Leave empty to use default (3)'),
                'caption' => $this->gT('Minimal character for search'),
            ),
            'selectQuestionByResponseMultiple' => array(
                'types' => 'Q',
                'i18n' => false,
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 300,
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->gT('In this case : id is saved with comma as separator by default.'),
                'caption' => $this->gT('Use a multiple option'),
            ),
            'selectQuestionByResponseMultipleSeparator' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 310,
                'inputtype' => 'text',
                'default' => ",",
                'help' => $this->gT('In case of multiple choice, use this caracter as multiple choice. default is <code>,</code>.'),
                'caption' => $this->gT('Separator for multiple choice'),
            ),
            'selectQuestionByResponseCategoriesForSurveys' => array(
                'types' => 'Q',
                'category' => $this->gT('Responses dropdown'),
                'sortorder' => 400,
                'inputtype' => 'textarea',
                'default' => "",
                'i18n' => true,
                'help' => $this->gT('If there are multiple surveys, you can have a different categorie for each. One line by value, Survey ID and categorie stroing separated by colon (“:“).'),
                'caption' => $this->gT('Optionnal categories for multiple surveys '),
            ),
        );
        $this->getEvent()->append('questionAttributes', $newAttributes);
    }

    /**
     * Control when save
     */
    public function beforeQuestionAttributeSave()
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }
        /* No need to check if have global permission */
        if (Permission::model()->hasGlobalPermission("surveys")) {
            return;
        }
        $model = $this->getEvent()->get('model');
        /* Only for attribute selectQuestionByResponseSurvey */
        if ($model->getAttribute('attribute') != 'selectQuestionByResponseSurvey') {
            return;
        }
        $model->setAttribute('value', trim($model->getAttribute('value')));
        /* Empty : always valid */
        if (empty($model->getAttribute('value'))) {
            return;
        }
        /* Find if same than previous*/
        $previous = "";
        if ($model->getAttribute('qaid')) {
            $previousModel = QuestionAttribute::model()->findByPk($model->getAttribute('qaid'));
            if ($previousModel) {
                $previous = trim($previousModel->getAttribute('value'));
            }
        }
        /* Not updated */
        if ($previous == $model->getAttribute('value')) {
            return;
        }
        /* explode */
        $surveysId = explode(",", $model->getAttribute("value"));
        $surveysId = array_filter(array_map(function ($surveyId) {
            $surveyId = intval($surveyId);
            if ($surveyId && Permission::model()->hasSurveyPermission($surveyId, "responses", "read")) {
                return $surveyId;
            }
            if ($surveyId) {
                App()->setFlashMessage(
                    sprintf(gT("You do not have sufficient right on survey %s"), $surveyId),
                    "warning"
                );
            }
        }, $surveysId));
        /* have permission */
        if (!empty($surveysId)) {
            $model->setAttribute("value", implode($surveysId));
        }
    }

    /**
    * Update Answer part in question display
    */
    public function beforeQuestionRender()
    {
        $questionRenderEvent = $this->getEvent();
        $qid = $questionRenderEvent->get('qid');

        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['selectQuestionByResponseSurvey'])) {
            return;
        }
        $baseSgq = $questionRenderEvent->get('surveyId') . "X" . $questionRenderEvent->get('gid') . "X" . $qid;
        $answer = $this->renderAnswer(
            $questionRenderEvent->get('surveyId'),
            $qid,
            $baseSgq,
            $aAttributes,
            $questionRenderEvent->get("answers")
        );
        if (empty($answer)) {
            return;
        }
        $vclass = "selectquestionbyresponses";
        $vtip = ""; //$this->gT("TEST"); // TODO
        $this->initJavascript();
        $questionRenderEvent->set("answers", $answer);
        $questionRenderEvent->set("class", $questionRenderEvent->get("class") . " selectquestionbyresponses-question");
    }

    public function newDirectRequest()
    {
        if ($this->getEvent()->get('target') != get_class($this)) {
            return;
        }
        $qid = App()->getRequest()->getQuery('qid');
        if (empty($qid)) {
            throw new CHttpException(400, gT("Invalid parameters."));
        }
        if (App()->getConfig('versionnumber') < 4) {
            $oQuestion = Question::model()->find("qid = :qid", array(':qid' => $qid));
        } else {
            $oQuestion = Question::model()->findByPk($qid);
        }
        if (empty($oQuestion)) {
            throw new CHttpException(404, gT("Question not found."));
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['selectQuestionByResponseSurvey'])) {
            throw new CHttpException(400, gT("Invalid question."));
        }
        if (empty($_SESSION['survey_' . $oQuestion->sid])) {
            throw new CHttpException(403, gT("Survey not started."));
        }
        if (empty($_SESSION['survey_' . $oQuestion->sid]['s_lang'])) {
            throw new CHttpException(403, gT("Survey not started."));
        }
        $aAttributes['selectQuestionByResponseQuestion'] = isset($aAttributes['selectQuestionByResponseQuestion']) ? trim($aAttributes['selectQuestionByResponseQuestion']) : "";
        if (empty($aAttributes['selectQuestionByResponseQuestion'])) {
            $this->errorOnParams($surveyid, "No title question for question {$qid}");
        }
        $surveyid = $oQuestion->sid;
        $result = [];
        $sourceSurveysId = explode(",", trim($aAttributes['selectQuestionByResponseSurvey']));
        $token = App()->getRequest()->getQuery('token');
        $aResponses = [];

        $aSurveysCategories = self::getSurveysCategories($aAttributes['selectQuestionByResponseCategoriesForSurveys'], $_SESSION['survey_' . $oQuestion->sid]['s_lang']);
        $aCategories = [];
        if (!empty($aSurveysCategories)) {
            foreach ($aSurveysCategories as $survey => $categorie) {
                $aCategories[$survey] = array(
                    'text' => $categorie,
                    'children' => []
                );
            }
        }
        foreach ($sourceSurveysId as $sourcesid) {
            $baseRequest = $this->getSurveysourceBaseRequest($sourcesid, $surveyid, $qid, $aAttributes);
            if ($baseRequest['error']) {
                $this->errorOnParams($surveyid, $baseRequest['error']);
            }
            $term = App()->getRequest()->getQuery('term');
            $ColumnTerm = $baseRequest['searchColumn'];
            $oCriteria = clone $baseRequest['criteria'];
            if ($term) {
                $oCriteria->compare(App()->db->quoteColumnName($ColumnTerm), $term, true);
            } else {
                $oCriteria->addCondition(App()->db->quoteColumnName($ColumnTerm) . " IS  NOT NULL and " . App()->db->quoteColumnName($ColumnTerm) . " <> ''");
            }
            /* dynamic filter */
            $aDynamicOtherFields = $this->getDynamicField($surveyid, $qid);
            if (!empty($aDynamicOtherFields)) {
                $useEmptyDynamicFilters = true;
                if (!empty($aAttributes['selectQuestionByResponseDynamicFiltersDisableEmpty'])) {
                    $useEmptyDynamicFilters = false;
                }
                foreach ($aDynamicOtherFields as $questionCode => $value) {
                    $column = self::getColumnByTitle($sourcesid, $questionCode);
                    if ($column) {
                        $columnQuoted = App()->db->quoteColumnName($column);
                        $requestvalue = App()->getRequest()->getparam($questionCode);
                        if ($requestvalue) {
                            //$partial = (strpos($requestvalue, "%") !== false OR strpos($requestvalue, "_") !== false);
                            $oCriteria->compare($columnQuoted, $requestvalue);
                        } elseif ($useEmptyDynamicFilters) {
                            $oCriteria->addCondition("$columnQuoted IS NULL or $columnQuoted = ''");
                        }
                        $aSelect[] = Yii::app()->db->quoteColumnName($column);
                    }
                }
            }
            $oResponses = Response::model($sourcesid)->findAll($oCriteria);
            foreach ($oResponses as $oResponse) {
                $responseId = $oResponse->id;
                if (count($sourceSurveysId) > 1) {
                    $responseId = $sourcesid . '-' . $oResponse->id;
                    $aCurrentReponse = array(
                        'id' => $responseId,
                        'text' => CHtml::encode($oResponse->getAttribute($ColumnTerm)),
                        'sid' => $sourcesid,
                        'srid' => $oResponse->id,
                    );
                } else {
                    $responseId = $oResponse->id;
                    $aCurrentReponse = array(
                        'id' => $oResponse->id,
                        'text' => CHtml::encode($oResponse->getAttribute($ColumnTerm)),
                    );
                }
                foreach ($baseRequest['codesToColumns'] as $code => $column) {
                    $aCurrentReponse[$code] = $oResponse->getAttribute($column);
                }
                if (isset($aCategories[$sourcesid])) {
                    $aCurrentReponse['group_text'] = CHtml::encode($aCategories[$sourcesid]['text']);
                    $aCategories[$sourcesid]['children'][] = $aCurrentReponse;
                } else {
                    $aResponses[] = $aCurrentReponse;
                }
            }
        }
        if (count($sourceSurveysId) > 1) {
            /* reorder by text */
            uasort($aResponses, function ($aResponseA, $aResponseB) {
                return strnatcasecmp($aResponseA['text'], $aResponseB['text']);
            });
        }
        if (empty($aCategories)) {
            /* Find the question code */
            $this->myRenderJson([
                'results' => $aResponses
            ]);
        } else {
            $aCategories = array_filter($aCategories);
            $this->myRenderJson([
                'results' => array_merge($aResponses, $aCategories)
            ]);
        }
    }

    /**
     * Get the criteria for surveyid
     * @param integer $sourcesid current source survey id
     * @param integer $surveyid survey id
     * @param integer $qid question id
     * @param array $attributes question attributes
     * @return array string in case of error. array with [searcColumn as string, \CDBCriteria fr base criteria]
     */
    private function getSurveysourceBaseRequest($sourcesid, $surveyid, $qid, $questionAttributes)
    {
        if (!empty($_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid])) {
            return $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid];
        }
        /* Returned array (to be moved in session survey) */
        $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid] = [
            'searchColumn' => null,
            'criteria' => null,
            'codesToColumns' => null,
            'error' => null,
        ];
        $oSurvey = Survey::model()->findByPk($sourcesid);
        if (empty($oSurvey)) {
            $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['error'] = sprintf($this->gT("Invalid survey %s for question %s."), $sourcesid, $qid);
            return $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid];
        }
        if (!$oSurvey->getHasResponsesTable()) {
            $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['error'] = sprintf($this->gT("Invalid survey %s for question %s, no response table."), $sourcesid, $qid);
            return $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid];
        }
        $criteriaTitleQuestion = new CDbCriteria();
        $criteriaTitleQuestion->select = ['sid','gid','qid','title'];
        $criteriaTitleQuestion->compare('sid', $sourcesid);
        $criteriaTitleQuestion->compare('title', $questionAttributes['selectQuestionByResponseQuestion']);
        $criteriaTitleQuestion->addInCondition('type', ["S","T","U","L","!","O","N","D","G","Y","*"]);
        $oTitleQuestion = Question::model()->find($criteriaTitleQuestion);
        if (!$oTitleQuestion) {
            $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['error'] = sprintf($this->gT("Question %s not found in survey %s"), $questionAttributes['selectQuestionByResponseQuestion'], $sourcesid);
            return $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid];
        }
        $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['searchColumn'] = $oTitleQuestion->sid . "X" . $oTitleQuestion->gid . "X" . $oTitleQuestion->qid;
        $oCriteria = new CDbCriteria();
        $aSelect = array(
            'id',
            'submitdate'
        );
        $aSelect[] = Yii::app()->db->quoteColumnName($_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['searchColumn']);
        $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['codesToColumns'] = [];
        $oCriteria->order = App()->db->quoteColumnName($_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['searchColumn']) . ' ASC, id ASC';
        $tokenUsage = $questionAttributes['selectQuestionByResponseTokenUsage'];
        if ($tokenUsage != 'no' && !$oSurvey->getIsAnonymized()) {
            if (empty($_SESSION['survey_' . $surveyid]['token'])) {
                $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['error'] = sprintf($this->gT("Token is mandatory to get response list on survey %s"), $sourcesid);
                return $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid];
            }
            $token = $_SESSION['survey_' . $surveyid]['token'];
            if ($tokenUsage == 'group' && class_exists('\responseListAndManage\Utilities')) {
                if (version_compare(App()->getConfig('TokenUsersListAndManageAPI'), "0.14", ">=")) {
                    $tokenList = \TokenUsersListAndManagePlugin\Utilities::getTokensList($sourcesid, $token, false);
                } else {
                    $tokenList = \responseListAndManage\Utilities::getTokensList($sourcesid, $token, false);
                }
                $oCriteria->addInCondition("token", $tokenList);
            } else {
                $oCriteria->compare("token", $token);
            }
        }
        /* fixed fields */
        $aFixedOtherFields = $this->getFixedField($surveyid, $qid);
        if (!empty($aFixedOtherFields)) {
            foreach ($aFixedOtherFields as $questionCode => $value) {
                $column = self::getColumnByTitle($sourcesid, $questionCode);
                if ($column) {
                    $columnQuoted = App()->db->quoteColumnName($column);
                    if ($value) {
                        $partial = (strpos($value, "%") !== false || strpos($value, "_") !== false);
                        $oCriteria->compare($columnQuoted, $value, $partial, 'AND', !$partial);
                    } else {
                        $oCriteria->addCondition("$columnQuoted IS NULL or $columnQuoted = ''");
                    }
                    $aSelect[] = Yii::app()->db->quoteColumnName($column);
                }
            }
        }
        /* contains fields */
        $aContainFilter = $this->getContainField($surveyid, $qid);
        if (!empty($aContainFilter)) {
            $separator = trim($questionAttributes['selectQuestionByResponseFiltersContainSeparator']);
            foreach ($aContainFilter as $questionCode => $value) {
                $column = self::getColumnByTitle($sourcesid, $questionCode);
                if ($column) {
                    $columnQuoted = App()->db->quoteColumnName($column);
                    if ($value) {
                        if ($separator) {
                            $containCriteria = new CDbCriteria();
                            $separator = mb_substr($separator, 0, 1);
                            $separator = self::escapeSqlString($separator); // Escape the separator
                            $containCriteria->compare($columnQuoted, $value); // IS the value
                            $value = self::escapeSqlString($value); // Escape the value
                            $containCriteria->compare($columnQuoted, $value . "{$separator}%", true, 'OR', false); // Start by
                            $containCriteria->compare($columnQuoted, "%{$separator}" . $value . "{$separator}%", true, 'OR', false); // contain
                            $containCriteria->compare($columnQuoted, "%{$separator}" . $value, true, 'OR', false); // End by
                            $oCriteria->mergeWith($containCriteria);
                        } else {
                            $value = self::escapeSqlString($value); // Escape the value
                            $oCriteria->compare($columnQuoted, "%" . $value . "%", true, 'AND', false); // contain wit the MSSQL issue
                        }
                    } else {
                        $oCriteria->addCondition("$columnQuoted IS NULL or $columnQuoted = ''");
                    }
                    $aSelect[] = Yii::app()->db->quoteColumnName($column);
                }
            }
        }
        /* dynamic filter */
        $aDynamicOtherFields = $this->getDynamicField($surveyid, $qid);
        if (!empty($aDynamicOtherFields)) {
            foreach ($aDynamicOtherFields as $questionCode => $value) {
                $column = self::getColumnByTitle($sourcesid, $questionCode);
                if ($column) {
                    $aSelect[] = Yii::app()->db->quoteColumnName($column);
                }
            }
        }

        $subQuestionCriteria = new \CDbCriteria();
        $subQuestionCriteria->select = ['title', 'question_order'];
        $subQuestionCriteria->compare('parent_qid', $qid);
        $subQuestionCriteria->order = 'question_order';
        if (App()->getConfig('versionnumber') < 4) {
            $subQuestionCriteria->compare('language', $oSurvey->language);
        }
        $subQuestions = Question::model()->findAll($subQuestionCriteria);
        $idQuestionFirst = array_shift($subQuestions);
        foreach ($subQuestions as $subQuestion) {
            $column = self::getColumnByTitle($sourcesid, $subQuestion->title);
            if ($column && $subQuestion->title != 'text') {
                $aSelect[] = Yii::app()->db->quoteColumnName($column);
                $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['codesToColumns'][$subQuestion->title] = $column;
            }
        }
        $aSelect = array_filter(array_unique($aSelect));
        $oCriteria->select = $aSelect;
        $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid]['criteria'] = $oCriteria;
        return $_SESSION['survey_' . $surveyid]['SelectQuestionByResponsesBaseRequest'][$qid][$sourcesid];
    }
    /**
     * When need update answers part
     */
    public function addTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        if (App()->getConfig('versionnumber') <= 5) {
            $viewPath = dirname(__FILE__) . "/twig_legacy5";
        }
        $this->getEvent()->append('add', array($viewPath));
    }

    private function initJavascript()
    {
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        //~ $min = (App()->getConfig('debug')) ? '.min' : '';
        $bsselect2package = intval(App()->getConfig('versionnumber') >= 6) ? 'select2-bootstrap' : 'bootstrap-select2';
        if (!Yii::app()->clientScript->hasPackage('limesurvey-selectquestionbyresponses')) {
            Yii::app()->clientScript->addPackage('limesurvey-selectquestionbyresponses', array(
                'basePath'    => get_class($this) . '.assets',
                'js'          => array('selectquestionbyresponses.js'),
                'css'          => array('selectquestionbyresponses.css'),
                'depends'     => array($bsselect2package),
            ));
        }
        /* Registering the package */
        Yii::app()->getClientScript()->registerPackage('limesurvey-selectquestionbyresponses');
    }

    private function renderAnswer($sid, $qid, $baseSgq, $aQuestionAttributes, $answers)
    {
        $this->subscribe('getPluginTwigPath', 'addTwigPath');
        $language = App()->getLanguage();
        if (App()->getConfig('versionnumber') < 4) {
            $oQuestion = Question::model()->find("qid = :qid", array(':qid' => $qid));
        } else {
            $oQuestion = Question::model()->findByPk($qid);
        }
        if (empty($oQuestion->subquestions)) {
            return;
        }
        $titleQuestion = isset($aQuestionAttributes['selectQuestionByResponseQuestion']) ? trim($aQuestionAttributes['selectQuestionByResponseQuestion']) : "";
        if (empty($titleQuestion)) {
            return;
        }
        $sourceSurveyids = explode(',', $aQuestionAttributes['selectQuestionByResponseSurvey']);
        $subQuestions = $oQuestion->subquestions;
        $oQuestionFirst = array_shift($subQuestions);
        if (App()->getConfig('versionnumber') >= 4) {
            $idQuestionFirstl10n =  QuestionL10n::model()->find(
                "qid = :qid and language = :language",
                array(':qid' => $oQuestionFirst->qid, ':language' => $language)
            );
        }
        /* @var string[] $currentValues */
        $currentValues = [];
        if (!empty($_SESSION["survey_$sid"][$baseSgq . $oQuestionFirst->title])) {
            $currentValue = $_SESSION["survey_$sid"][$baseSgq . $oQuestionFirst->title];
            if (boolval($aQuestionAttributes['selectQuestionByResponseMultiple'])) {
                $currentValues = explode($aQuestionAttributes['selectQuestionByResponseMultipleSeparator'], $currentValue);
            } else {
                $currentValues = array($currentValue);
            }
        }
        /* @var array[] $currentOptions */
        $currentOptions = array_fill_keys($currentValues, $currentValues);
        /* Multiple data by question attributes */
        $placeholder = $this->gT("Search");
        if (!empty($aQuestionAttributes['selectQuestionByResponsePlaceholder'][$language])) {
            $placeholder = $aQuestionAttributes['selectQuestionByResponsePlaceholder'][$language];
        }
        $minimumInputLength = 3;
        if (isset($aQuestionAttributes['selectQuestionByResponseMinChar']) && $aQuestionAttributes['selectQuestionByResponseMinChar'] !== '') {
            if (strval(intval(trim($aQuestionAttributes['selectQuestionByResponseMinChar']))) === trim($aQuestionAttributes['selectQuestionByResponseMinChar'])) {
                $minimumInputLength = intval($aQuestionAttributes['selectQuestionByResponseMinChar']);
            }
        }
        $fixedFilter = array();
        if (!empty($aQuestionAttributes['selectQuestionByResponseFilters'])) {
            $fixedFilter = $this->setFixedField($qid, $aQuestionAttributes['selectQuestionByResponseFilters'], $sid);
        }
        $containFilter = array();
        if (!empty($aQuestionAttributes['selectQuestionByResponseFiltersContain'])) {
            $containFilter = $this->setContainField($qid, $aQuestionAttributes['selectQuestionByResponseFiltersContain'], $sid);
        }
        $dynamicFilter = array();
        if (!empty($aQuestionAttributes['selectQuestionByResponseDynamicFilters'])) {
            $dynamicFilter = $this->setDynamicField($qid, $aQuestionAttributes['selectQuestionByResponseDynamicFilters'], $sid, true);
        }
        $scriptParams = array(
            'plugin' => get_class($this),
            'function' => 'getData',
            'qid' => $qid,
            'lang' => App()->getLanguage()
        );
        if (!empty($_SESSION["survey_$sid"]['token'])) {
            $scriptParams['token'] = $_SESSION["survey_$sid"]['token'];
        }
        $aSurveysCategories = self::getSurveysCategories($aQuestionAttributes['selectQuestionByResponseCategoriesForSurveys'], App()->getLanguage());
        foreach ($sourceSurveyids as $sourcesid) {
            /* @var array : copy of current values deleted already found for multiple source sid */
            $cleanedCurrentValues = $currentValues;
            if (!empty($currentValues)) {
                $oSourceSurvey = Survey::model()->findByPk($sourcesid);
                if (empty($oSourceSurvey)) {
                    continue;
                }
                if (!$oSourceSurvey->getHasResponsesTable()) {
                    continue;
                }
                /* title question */
                $criteriaTitleQuestion = new CDbCriteria();
                $criteriaTitleQuestion->select = ['sid','gid','qid','title'];
                $criteriaTitleQuestion->compare('sid', $sourcesid);
                $criteriaTitleQuestion->compare('title', $titleQuestion);
                $criteriaTitleQuestion->addInCondition('type', ["S","T","U","L","!","O","N","D","G","Y","*"]);
                $oTitleQuestion = Question::model()->find($criteriaTitleQuestion);
                if (!$oTitleQuestion) {
                    continue;
                }
                /* @todo minimize the response request */
                /* find the current id */
                $sTitle = $oTitleQuestion->sid . "X" . $oTitleQuestion->gid . "X" . $oTitleQuestion->qid;
                $aCodesToColumns = array();
                foreach ($subQuestions as $subQuestion) {
                    $column = self::getColumnByTitle($sourcesid, $subQuestion->title);
                    if ($column && $subQuestion->title != 'text') {
                        $aSelect[] = Yii::app()->db->quoteColumnName($column);
                        $aCodesToColumns[$subQuestion->title] = $column;
                    }
                }
                foreach ($currentValues as $id) {
                    if (count($sourceSurveyids) > 1) {
                        $oResponse = null;
                        $aIds = explode('-', $id);
                        if (count($aIds) == 2 && $aIds[0] == $sourcesid) {
                            $oResponse = Response::model($sourcesid)->findByPk($aIds[1]);
                        }
                    } else {
                        $oResponse = Response::model($sourcesid)->findByPk($id);
                    }
                    if ($oResponse) {
                        $responseId = $oResponse->id;
                        $text = CHtml::encode(CHtml::encode($oResponse->getAttribute($sTitle)));
                        if (count($sourceSurveyids) > 1) {
                            if (isset($aSurveysCategories[$sourcesid])) {
                                $text = CHtml::encode("<strong>[" . CHtml::encode($aSurveysCategories[$sourcesid]) . "]</strong> ") . $text;
                            }
                            $currentOptions[$id] = array(
                                'id' => $sourcesid . '-' . $oResponse->id,
                                'text' => $text,
                                'data' => [
                                    'sid' => $sourcesid,
                                    'srid' => $oResponse->id,
                                ]
                            );
                        } else {
                            $currentOptions[$id] = array(
                                'id' => $oResponse->id,
                                'text' => $text,
                                'data' => []
                            );
                        }
                        foreach ($aCodesToColumns as $code => $column) {
                            $currentOptions[$id]['data'][$code] = $oResponse->getAttribute($column);
                        }
                        unset($cleanedCurrentValues[array_search($id, $cleanedCurrentValues)]);
                    }
                }
            }
            $currentValues = $cleanedCurrentValues;
        }
        $scriptOptions = array(
            'serviceUrl' => App()->getController()->createUrl(
                'plugins/direct',
                $scriptParams
            ),
            'placeholder' => $placeholder,
            'multiple' => boolval($aQuestionAttributes['selectQuestionByResponseMultiple']),
            'minimumInputLength' => $minimumInputLength,
            'language' => $language, // Need to register language script too, todo : create limesurvey lang to slect2 lang
            'prefillData' => []
        );
        $translation = array(
            "You need javascript activated to answer to this question." => $this->gT("You need javascript activated to answer to this question."),
        );
        if (App()->getConfig('versionnumber') < 4) {
            $renderData = array(
                'sid' => $sid,
                'qid' => $qid,
                'baseSgq' => $baseSgq,
                'currentOptions' => $currentOptions,
                'label' => $oQuestionFirst->question,
                'aQuestionAttributes' => $aQuestionAttributes,
                'scriptOptions' => $scriptOptions,
                'coreanswers' => $answers,
                'fixedFilter' => $fixedFilter,
                'containFilter' => $containFilter,
                'dynamicFilter' => $dynamicFilter,
                'translation' => $translation,
                'aSurveyInfo' => getSurveyInfo($sid, App()->getLanguage())
            );
        } else {
            $renderData = array(
                'sid' => $sid,
                'qid' => $qid,
                'baseSgq' => $baseSgq,
                'currentOptions' => $currentOptions,
                'label' => $idQuestionFirstl10n->question,
                'aQuestionAttributes' => $aQuestionAttributes,
                'scriptOptions' => $scriptOptions,
                'coreanswers' => $answers,
                'fixedFilter' => $fixedFilter,
                'containFilter' => $containFilter,
                'dynamicFilter' => $dynamicFilter,
                'translation' => $translation
            );
        }
        return App()->twigRenderer->renderPartial(
            './survey/questions/answer/multipleshorttext/dropdownresponses.twig',
            $renderData
        );
    }

    /**
     * Set the other field for current qid
     * @param integer $qid
     * @param string $otherField to analyse
     * @param integer $surveyId in this survey
     * @return array
     */
    private function setFixedField($qid, $otherField, $surveyId)
    {
        $aOtherFieldsLines = preg_split('/\r\n|\r|\n/', $otherField, -1, PREG_SPLIT_NO_EMPTY);
        $aOtherFields = array();
        foreach ($aOtherFieldsLines as $otherFieldLine) {
            if (!strpos($otherFieldLine, ":")) {
                continue; // Invalid line
            }
            $key = substr($otherFieldLine, 0, strpos($otherFieldLine, ":"));
            $value = substr($otherFieldLine, strpos($otherFieldLine, ":") + 1);
            $value = self::qesEMProcessString($value, true);
            $aOtherFields[$key] = $value;
        }
        $_SESSION["survey_{$surveyId}"]["questionByResponseFixedField"][$qid] = $aOtherFields;
        return $aOtherFields;
    }

    /**
     * get the fixed other field
     * @param $sid
     * @param $qid
     * @return array|null
     */
    private function getFixedField($surveyId, $qid)
    {
        if (empty($_SESSION["survey_{$surveyId}"]["questionByResponseFixedField"][$qid])) {
            return null;
        }
        return $_SESSION["survey_{$surveyId}"]["questionByResponseFixedField"][$qid];
    }

    /**
     * Set the other field for current qid
     * @param integer $qid
     * @param string $containField to analyse
     * @param integer $surveyId in this survey
     * @param boolean $static static value
     * @return array
     */
    private function setContainField($qid, $containField, $surveyId)
    {
        if (!strpos($containField, ":")) {
            return; // Invalid value
        }
        $key = substr($containField, 0, strpos($containField, ":"));
        $value = substr($containField, strpos($containField, ":") + 1);
        $value = self::qesEMProcessString($value, true);
        $containField = array($key => $value);

        $_SESSION["survey_{$surveyId}"]["questionByResponseContaindField{$qid}"] = $containField;
        return $containField;
    }

    /**
     * get the fixed other field
     * @param $sid
     * @param $qid
     * @return array|null
     */
    private function getContainField($surveyId, $qid)
    {
        if (empty($_SESSION["survey_{$surveyId}"]["questionByResponseContaindField{$qid}"])) {
            return null;
        }
        return $_SESSION["survey_{$surveyId}"]["questionByResponseContaindField{$qid}"];
    }

    /**
     * Set the other dynamicfield for current qid
     * @param integer $qid
     * @param string $otherField to analyse
     * @param integer $surveyId in this survey
     * @param boolean $static static value
     * @return array
     */
    private function setDynamicField($qid, $otherField, $surveyId)
    {
        $aOtherFieldsLines = preg_split('/\r\n|\r|\n/', $otherField, -1, PREG_SPLIT_NO_EMPTY);
        $aOtherFields = array();
        foreach ($aOtherFieldsLines as $otherFieldLine) {
            if (!strpos($otherFieldLine, ":")) {
                continue; // Invalid line
            }
            $key = substr($otherFieldLine, 0, strpos($otherFieldLine, ":"));
            $value = substr($otherFieldLine, strpos($otherFieldLine, ":") + 1);
            $value = self::qesEMProcessString($value, false);
            $aOtherFields[$key] = $value;
        }
        $_SESSION["survey_{$surveyId}"]["questionByResponseDynamicField{$qid}"] = $aOtherFields;
        return $aOtherFields;
    }

    /**
     * get the dynamic other field
     * @param $sid
     * @param $qid
     * @return array|null
     */
    private function getDynamicField($surveyId, $qid)
    {
        if (empty($_SESSION["survey_{$surveyId}"]["questionByResponseDynamicField{$qid}"])) {
            return null;
        }
        return $_SESSION["survey_{$surveyId}"]["questionByResponseDynamicField{$qid}"];
    }

    /**
     * Log or throw error and quit
     * @param string
     * @return void
     */
    private function errorOnParams($sid, $errorMessage)
    {
        $this->log($errorMessage, 'warning');
        if (Permission::model()->hasSurveyPermission($sid, 'content')) {
            $this->myRenderJson([
                'results' => [
                    [
                        'id' => 0,
                        'text' => "[" . $this->gT('Error') . "] " . $errorMessage
                    ]
                ]
            ]);
        }
        $this->myRenderJson([
            'results' => []
        ]);
    }

    /**
     * render json
     * @param mixed
     * @return void
     */
    private function myRenderJson($data = null)
    {
        Yii::import('application.helpers.viewHelper');
        viewHelper::disableHtmlLogging();
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($data);
        Yii::app()->end();
    }

    /**
     * Process a string via expression managerand API independant
     * @param string $string
     * @param boolean $static
     * @return string
     */
    private static function qesEMProcessString($string, $static = true)
    {
        $replacementFields = array();
        if (version_compare(Yii::app()->getConfig('versionnumber'), "3.6.2", ">=")) {
            return \LimeExpressionManager::ProcessStepString($string, $replacementFields, 3, $static);
        }
        if (intval(Yii::app()->getConfig('versionnumber')) < 3) {
            return \LimeExpressionManager::ProcessString($string, null, $replacementFields, false, 3, 0, false, false, $static);
        }
        /* between 3 and 3.6.2 */
        return \LimeExpressionManager::ProcessString($string, null, $replacementFields, 3, 0, false, false, $static);
    }

    /**
     * Return the question column accoring to sid and title
     * @param integer $sid
     * @param string title
     * @return string|false
     */
    private static function getColumnByTitle($sid, $title)
    {
        $oQuestion = Question::model()->find(
            "sid=:sid and title=:title and parent_qid=0",
            array(
                ":sid" => $sid,
                ":title" => $title
            )
        );
        if (empty($oQuestion)) {
            return false;
        }
        if (!in_array($oQuestion->type, array("5","D","G","I","L","N","O","S","T","U","X","Y","!","*"))) {
            return false;
        }
        return "{$oQuestion->sid}X{$oQuestion->gid}X{$oQuestion->qid}";
    }

    /**
     * Transform selectQuestionByResponseCategoriesForSurveys attribute to array in language
     * Line by line, separated by a :
     * @param string[] The attribute
     * @param string language to get
     * @return array
     */
    private static function getSurveysCategories($attribute, $language)
    {
        if (empty($attribute[$language])) {
            return array();
        }
        $attribute = trim($attribute[$language]);
        $aCategoriesLines = preg_split('/\r\n|\r|\n/', $attribute, -1, PREG_SPLIT_NO_EMPTY);
        $aSurveysCategories = array();
        foreach ($aCategoriesLines as $aCategoriesLine) {
            if (!strpos($aCategoriesLine, ":")) {
                continue; // Invalid line
            }
            $survey = substr($aCategoriesLine, 0, strpos($aCategoriesLine, ":"));
            $text = substr($aCategoriesLine, strpos($aCategoriesLine, ":") + 1);
            $aSurveysCategories[$survey] = $text;
        }
        return $aSurveysCategories;
    }

    /**
     * Sql string must be escaped diffrently
     * @see https://github.com/yiisoft/yii2/blob/master/framework/db/mssql/conditions/LikeConditionBuilder.php
     * @param string $string
     * @return string
     */
    private static function escapeSqlString($string)
    {
        $escapingReplacements = [
            '%' => '\%',
            '_' => '\_',
            '\\' => '\\\\',
        ];
        if (in_array(App()->db->driverName, ['sqlsrv', 'dblib', 'mssql'])) {
            $escapingReplacements = [
                '%' => '[%]',
                '_' => '[_]',
                '[' => '[[]',
                ']' => '[]]',
                '\\' => '[\\]',
            ];
        }
        return strtr($string, $escapingReplacements);
    }
}
